# joybook

> GranblueFantasy Joybook

#### Build Setup

``` bash
# install dependencies
npm install

# install global dependencies
npm install -g typescript tslint eslint

# serve with hot reload at localhost:9080
npm run dev

# build electron application for production
npm run build


# lint all JS/Vue component files in `src/`
npm run lint

```
