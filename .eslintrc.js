module.exports = {
	root: true,
	parser: 'babel-eslint',
	parserOptions: {
		sourceType: "module"
	},
	env: {
		browser: true,
		node: true
	},
	extends: 'airbnb-base',
	globals: {
		__static: true
	},
	plugins: [
		'html'
	],
	rules: {
		'import/no-unresolved': 0,
		'no-param-reassign': 0,
		'no-shadow': 0,
		'import/extensions': 0,
		'import/newline-after-import': 0,
		'import/prefer-default-export': 0,
		'import/no-extraneous-dependencies': 0,
		'no-multi-assign': 0,
		'indent': ['error', 'tab'],
		'linebreak-style': ['error', 'unix'],
		'class-methods-use-this': 0,
		'no-return-assign': 0,
		'max-len': [2, 140, 4, {
			'ignoreUrls': true,
			'ignoreTemplateLiterals': true,
			'ignoreComments': true,
			'ignoreTrailingComments': true,
			'ignoreRegExpLiterals': true,
			'ignoreStrings': true
		}],
		'no-restricted-globals': 1,
		'consistent-return': 0,
		'no-unused-expressions': 0,
		'no-nested-ternary': 0,
		'no-plusplus': 0,
		'no-await-in-loop': 0,
		'global-require': 0,
		'no-underscore-dangle': 0,
		'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
		'no-console': process.env.NODE_ENV === 'production' ? 0 : 0,
		'no-tabs': 0,
		'func-names': 1,
		'comma-dangle': [2, 'only-multiline'],
	}
}
