import Vue from 'vue';
import LocalForage from 'localforage';
import { AxiosStatic } from 'axios';

import { JoyIconSymbol } from '../src/renderer/app/ui/joyicons';

interface CustomLocalForageDriver extends LocalForage {
	mergeItem<T>(key: string, value: T, callback?: Function): Promise<T>;
	overrideItem<T>(key: string, value: T, callback?: Function): Promise<T>;
}

declare module 'vue/types/vue' {
	export interface Vue {
		readonly _uid: number;
		readonly JoyIconSymbol: JoyIconSymbol;
		readonly $localforage: CustomLocalForageDriver;
		readonly $http: AxiosStatic;
	}
}
