
const state = {
	main: 0,
};

interface ICounterState {
	main: number;
}

const mutations = {
	DECREMENT_MAIN_COUNTER(state: ICounterState) {
		state.main -= 1;
	},
	INCREMENT_MAIN_COUNTER(state: ICounterState) {
		state.main += 1;
	},
};

const actions = {
	someAsyncTask(commit: { commit: Function }) {
		// do something async
		commit.commit('INCREMENT_MAIN_COUNTER');
	},
};

export default {
	state,
	mutations,
	actions,
};
