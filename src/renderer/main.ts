// Vue
import Vue from 'vue';
import Vuetify from 'vuetify';

// Functional
import axios from 'axios';
import { remote } from 'electron';
import * as localforage from 'localforage';

// App
import App from './App.vue';
import component from './app/index';
import router from './router';
import store from './store';

// Styles
// @ts-ignore
import colors from 'vuetify/es5/util/colors';
import './styles/joybook.scss';

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.use(component);
Vue.use(Vuetify, {
	theme: {
		primary: colors.grey.darken4,
		accent: colors.grey.lighten4,
	},
});

localforage.config({
		driver: localforage.INDEXEDDB,
		name: 'YukinoshitaYukinoWaGaAi',
		storeName: 'ShibuyaRinAlsoWaGaAi',
		description: '谁也别想抢',
		version: 1,
});
localforage.ready() // 检查数据库是否准备完毕
	.then(() => Vue.prototype.$localforage = localforage)
	.then(() => Vue.prototype.$http = axios)
	.then(() => Vue.config.productionTip = false)
	.then(() => new Vue({
		components: { App },
		router,
		store,
		template: '<App/>',
	}).$mount('#app'))
	.catch((error) => remote.dialog.showErrorBox('localforage', error));
