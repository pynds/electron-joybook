import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

const home = (r: Function) => require.ensure([], () => r(require('../pages/index')));

export default new Router({
	routes: [
		{
			path: '/',
			name: 'landing-page',
			component: home,
		},
		{
			path: '/settings',
			name: 'settings',
		},
		{
			path: '*',
			redirect: '/',
		},
	],
});
