import { VueConstructor } from 'vue';
import * as components from './ui';

const Joybook = {
	install(Vue: VueConstructor) {
		(function registerComponents(components: any) {
			if (components) {
				for (const key in components) {
					const component = components[key];

					if (component.$_mixins) {
						Vue.mixin(component.$_mixins);
					}
					if (!registerComponents(component.$_subcomponents)) {
						Vue.component(key, component);
					}
				}
				return true;
			}
			return false;
		})(components as any);
	},
};

export default Joybook;
