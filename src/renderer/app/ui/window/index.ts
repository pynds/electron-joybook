import JoyTitlebarControls from './title-bar-controls.vue';
import JoyTitlebarIcon from './title-bar-icon.vue';
import JoyTitlebarTitle from './title-bar-title.vue';
import JoyTitlebar from './title-bar.vue';
import JoyWindowControlsButton from './window-controls.vue';

export {
	JoyTitlebarControls,
	JoyTitlebarIcon,
	JoyTitlebarTitle,
	JoyTitlebar,
	JoyWindowControlsButton,
};

export default {
	$_subcomponents: {
		JoyTitlebar,
		JoyTitlebarControls,
		JoyTitlebarIcon,
		JoyTitlebarTitle,
		JoyWindowControlsButton,
	},
};
