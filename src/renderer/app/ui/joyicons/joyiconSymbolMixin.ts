import Vue from 'vue';
import Component from 'vue-class-component';
import JoyIconSymbol from './joyicon.generated';

@Component
export default class JoyIconSymbolMixin extends Vue {
	public joyIconSymbol: JoyIconSymbol;
}
