export default class JoyiconSymbol {
	public constructor(public w: number, public h: number, public d: string) {}

	public static get joyicon() {
		return new JoyiconSymbol(
			16,
			16,
			'data:image/x-icon;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAABILAAASCwAAAAAAAAAAAABCSmoAQUlpH09be9hkc5H/rMHf/7HI6v+Kmrf/XmeC/0lFh/96Ztz/kYDr/4+C2/9WXoX/RE5s/1FefP9QXXv/T1t5AEhTclpRXXz8aniU/5utyv+vxuf/Z3KQ/2t2lv9xf67/lKbe/5Om4f+NnOb/a3ip/1Bdff9HUXH/U2B//09begBBSWmFQUpp/z1DXv9wepP/o7bV/6K32v+1yur/xNv3/8Pb9//B2PX/pr/s/21/r/9UYYL/VmSE/09bev84Pl4IOD5drjE0Uf8uK1H/anOa/6/F5v+4z/D/x975/6/E6v+nuu//vtT2/8fe+v+MnLr/ODxX/09aeP9WZIL/NTpYITI2U90+RGX/cH2u/6u/7f+2zfn/u9L5/8Tb+f++1PT/sMXx/7DF6v/F3Pn/wdfy/2Rthf8yNlL/Tll3/yorRCMyNlTfZ3mv/5yx2v++1Pj/s8n5/7bM+P++1vj/wtn4/8Xc+f/A1/f/v9b4/8Pb+/+nudT/NzpV/zI1Uf8qKkZCNThZ63ODrP91gpz/iY6T/6Ovvf+70vb/wdn5/8LZ+P/A1/n/uc/4/7XM+P+4z/n/udDy/09Vb/8nKEL/NjpY0k5XeP+gtdf/jJyz/y0uMP9RUk7/vNDq/8be+v/F3Pn/v9b5/7bO+f+yyfr/scj5/7nQ+P+Ci7P/RTxs/0hScf9kc5z/cYKn/5Kiwf9vepD/bnyX/7/W9P/H3vr/xt35/8bc+/+1xt7/p7TH/7XI6P+/1fj/tMnw/5Si1P9RXX3/XWuQ/2Ryj/9RXHn/four/6O21f+ovNf/vtTv/8jf+/+twuP/am93/zw3Kf93fof/jJ27/7HJ7/+Vr+3/S1V2/1Nffv9WYoL/UV16/3B/of9gbIv/WWaE/4OTsv+yx+T/kKLG/19rgP9fanz/hpWr/6K20P+juNv/la/t/0xXdv9QXHv/UV17/2R0mP9PWn3/T1t5/1RhgP9fa4//fY6y/3iGq/9seJn/jp29/7nO6P+3zOj/hJOv/5iqy/9SX3//VGKC/1Zkg/9baIz/TVh2/1Vjgf9TYH7/WmeI/2Z1l/9ebY//UV14/257mP+60Oj/l6jH/z5EYv9ESmb/VGGA/1Fdff9ba47/U2CC/1Jfff9WZIL/VGJ//1pnif9aaIr/V2WG/1hkg/9te5j/uM7q/2Zyk/83PVv/Rk9t/1Zkgv9OWnj/VGKA/1Ngf/9cbI//W2uM/1hmhv9XZIf/UFx7/1Jffv9RXXv/cH+f/4SUt/9LVnX/RE1r/1JefP9SX33/VGF//1Rhf/9UYX//W2uM/1trjP9ZaIv/WGaJ/1Bcff9ZaIr/WWeI/2V1nP9YZoj/UFx6/0tWdf9aaYnxgAAAAIAAAACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==',
		);
	}

	public static get alert() {
		return new JoyiconSymbol(
			16,
			16,
			'M8.865 1.52c-.18-.31-.51-.5-.87-.5s-.69.19-.87.5L.275 13.5c-.18.31-.18.69 0 1 .19.31.52.5.87.5h13.7c.36 0 .69-.19.86-.5.17-.31.18-.69.01-1L8.865 1.52zM8.995 13h-2v-2h2v2zm0-3h-2V6h2v4z',
		);
	}

	public static get kebabHorizontal() {
		return new JoyiconSymbol(
			13,
			16,
			'M1.5 9a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3zm5 0a1.5 1.5 0 1 1 0-3 1.5 1.5 0 0 1 0 3z',
		);
	}

	public static get settings() {
		return new JoyiconSymbol(
			16,
			16,
			'M4 7H3V2h1v5zm-1 7h1v-3H3v3zm5 0h1V8H8v6zm5 0h1v-2h-1v2zm1-12h-1v6h1V2zM9 2H8v2h1V2zM5 8H2c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1zm5-3H7c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1zm5 4h-3c-.55 0-1 .45-1 1s.45 1 1 1h3c.55 0 1-.45 1-1s-.45-1-1-1z',
		);
	}
}
