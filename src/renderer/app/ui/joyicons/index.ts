import JoyIconSymbol from './joyicon.generated';
import JoyIcon from './joyicon.vue';
import JoyIconSymbolMixin from './joyiconSymbolMixin';

export { JoyIcon, JoyIconSymbol, JoyIconSymbolMixin };

export default {
	$_subcomponents: {
		JoyIcon,
	},
	$_mixins: {
		data() {
			return {
				JoyIconSymbol,
			};
		},
	},
};
