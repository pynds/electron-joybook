import JoyCellItem from './cell-item.vue';
import JoyCell from './cell.vue';

export { JoyCell, JoyCellItem };

export default {
	$_subcomponents: {
		JoyCellItem,
		JoyCell,
	},
};
