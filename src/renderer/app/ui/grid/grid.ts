import { CreateElement, RenderContext } from 'vue/types';

export default function Grid(name: string) {
	return {
		name: `joy-${name}`,

		functional: true,

		props: {
			id: String,
			tag: {
				type: String,
				default: 'div',
			},
		},

		render: (h: CreateElement, { props, data, children }: RenderContext) => {
			data.staticClass = `${name} ${data.staticClass || ''}`.trim();

			if (data.attrs) {
				const classes = Object.keys(data.attrs).filter((key) => {
					if (key === 'slot') return false;

					const value = data.attrs[key];
					return value || typeof value === 'string';
				});

				if (classes.length) data.staticClass += ` ${classes.join(' ')}`;
				delete data.attrs;
			}

			if (props.id) {
				data.domProps = data.domProps || {};
				data.domProps.id = props.id;
			}

			return h(props.tag, data, children);
		},
	};
}
