import JoyContainer from './Container';
import JoyFlex from './Flex';
import JoyLayout from './Layout';

import { createSimpleFunctional } from '../../util/helpers';

const JoySpacer = createSimpleFunctional('spacer');

export { JoyContainer, JoyLayout, JoyFlex, JoySpacer };

export default {
	$_subcomponents : {
		JoyContainer,
		JoyFlex,
		JoyLayout,
		JoySpacer,
	},
};
