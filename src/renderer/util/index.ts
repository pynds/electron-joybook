export interface IEqualsObject {
	[index: string]: any;
}

/**
 * Performs a deep comparison between two values to determine if they are equivalent.
 */
export function equals(a: IEqualsObject, b: IEqualsObject): boolean {
	if (a === b) return true;
	if (a instanceof Date && b instanceof Date) return a.getTime() === b.getTime();
	if (!a || !b || (typeof a !== 'object' && typeof b !== 'object')) return a === b;
	if (a === null || a === undefined || b === null || b === undefined) return false;
	if (a.prototype !== b.prototype) return false;
	const keys = Object.keys(a);
	if (keys.length !== Object.keys(b).length) return false;
	return keys.every((k) => equals(a[k], b[k]));
}

export function generateId(): number { return Math.floor(Math.random() * 10000); }
