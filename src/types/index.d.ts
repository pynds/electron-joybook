/**
 * stage.gGameStatus
 * any 类型的基本都是我不确定的
 * 因为有些需要操作才能知道到底干嘛的 懒得搞
 */
export interface IGBFGameStatus {
	$use_ability: any;
	abilityRailDisp: number;
	abilityRailUse: number;
	ability_pick: any;
	ability_popup: number;
	ability_sub_param: any[];
	// 也许返回的不止是string
	action: {
		[index: string]: string;
		ab_select: string;
	};
	already_finish: boolean;
	arcarumStageEffectDeferred: {
		[index: string]: Function;
		always: () => any;
		done: () => any;
		fail: () => any;
		notify: () => any;
		notifyWith: (a: any, b: any) => any;
		pipe: () => any;
		progress: () => any;
		promise: (a: any) => any;
		reject: () => any;
		rejetWith: (a: any, b: any) => any;
		resolve: () => any;
		resolveWith: (a: any, b: any) => any;
		state: () => any;
		then: () => any;
	};
	assist: {
		all: number;
		friend: number;
		guild: number;
	};
	attackQueue: {
		$useAbility: any[];
		$useAbilityTmp: any;
		abilityRailUI: any;
		attackButtonPushed: boolean;
		charaChangeFlag: boolean;
		index: any[];
		param: any[];
		pause: boolean;
	};
	attack_action: any;
	attack_count: number;
	attacking: number;
	auto_attack: boolean;
	auto_attack_display_flag: number;
	backImage: {
		[index: string]: any;
	};
	backImageValue: any[];
	balloon: string;
	battle_end: boolean;
	boss: { [index: string]: any };
	bossmode: { [index: string]: any };
}
