@echo off
setlocal ENABLEDELAYEDEXPANSION

set path=%~dp0
set userFile=2
set userFile[0].name=mainfile
set userFile[1].name=subfile
set userFile[2].name=subfile
set userFile[0].lnkname=MamayuScript
set userFile[1].lnkname=MamayuScript-CoopUse
set userFile[3].lnkname=MamayuScript-CoopUse

for /l %%i in (0,1,2) do (
    call :lnk !userFile[%%i].lnkname! !userFile[%%i].name!
)

exit

:lnk

::设置程序或文件的完整路径（必选）

set Program=C:\Program Files (x86)\Google\Chrome\Application\chrome.exe

::设置快捷方式名称（必选）

set LnkName=%1

::设置程序的工作路径，一般为程序主目录，此项若留空，脚本将自行分析路径

set WorkDir=

::设置快捷方式显示的说明（可选）

set Desc=ver0.1

if not defined WorkDir call:GetWorkDir "%Program%"

(echo Set WshShell=CreateObject("WScript.Shell"^)

echo strDesKtop=WshShell.SpecialFolders("DesKtop"^)

echo Set oShellLink=WshShell.CreateShortcut(strDesKtop^&"\%LnkName%.lnk"^)

echo oShellLink.Arguments="--user-data-dir=%path%%2"

echo oShellLink.TargetPath="%Program%"

echo oShellLink.WorkingDirectory="%WorkDir%"

echo oShellLink.WindowStyle=1

echo oShellLink.Description="%Desc%"

echo oShellLink.Save)>makelnk.vbs

makelnk.vbs

del /f /q makelnk.vbs

goto :eof

:GetWorkDir

set WorkDir=%~dp1

set WorkDir=%WorkDir:~,-1%

goto :eof