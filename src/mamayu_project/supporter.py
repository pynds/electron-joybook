import port
import var
chrome = port.chrome
js = port.javaScript
elementAttribute = port.elementAttribute
elementInnerText = port.elementInnerText
elementStatement = port.elementStatement
click = port.click
goto = port.goto

def supporter(supType, supName1, supName2):
    supporterNum = js('return document.getElementsByClassName("prt-supporter-list prt-module")[0].getElementsByClassName("prt-supporter-attribute")[%s].children.length' % (supType))
    main = None
    if supporterNum != None:
        for i in range(supporterNum):
            dataImage = port.elementAttribute('//*[@id="cnt-quest"]/div[2]/div[%s]/div[%s]/div[2]/div[1]' % (int(supType)+3, i+1), 'data-image')
            if dataImage == supName1:
                port.elementMoveTop('//*[@id="cnt-quest"]/div[2]/div[%s]/div[%s]' % (int(supType)+3, i+1))
                port.time.sleep(port.random.uniform(0.7, 1.4))
                port.click(
                    '//*[@id="cnt-quest"]/div[2]/div[%s]/div[%s]/div[2]' % (int(supType)+3, i+1),
                    'sup'
                )
                main = True
                break
        if main != True:
            for i in range(supporterNum):
                dataImage = port.elementAttribute('//*[@id="cnt-quest"]/div[2]/div[%s]/div[%s]/div[2]/div[1]' % (int(supType)+3, i+1), 'data-image')
                if dataImage == supName2:
                    port.elementMoveTop('//*[@id="cnt-quest"]/div[2]/div[%s]/div[%s]' % (int(supType)+3, i+1))
                    port.time.sleep(port.random.uniform(0.7, 1.4))
                    port.click(
                        '//*[@id="cnt-quest"]/div[2]/div[%s]/div[%s]/div[2]' % (int(supType)+3, i+1),
                        'sup'
                    )
                    break

def typeSelect(supType, apNum):
    if supType == '0':
        supType = 7
    if port.isElement('//*[@id="cnt-quest"]/div[2]/div[3]/div[1]') != None:
        if port.elementAttribute('//*[@id="prt-type"]/div[%s]' % (supType), 'class') == 'icon-supporter-type-%s btn-type selected' % (supType):
            if port.isElement('//*[@id="wrapper"]/div[3]/div[3]/div[3]/div[2]') != None:
                port.click(
                    '//*[@id="wrapper"]/div[3]/div[3]/div[3]/div[2]',
                    'btn'
                )
            elif var.switch == False:
                supporter(var.ptType, var.sup1, var.sup2)
                if port.elementAttribute('//*[@id="loading"]/div', 'style') == 'display: block;':
                    var.switch = True
        else:
            port.click(
                '//*[@id="prt-type"]/div[%s]/div' % (supType),
                'btn'
            )
        if port.elementInnerText('//*[@id="pop"]/div/div[1]') == 'APが足りません':
            js('window.location.href = %s%s%s' % ('"', var.item, '"'))
            while js('return window.location.hash.split("/")[0]') == var.item:
                if elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[2]/div[1]/div[1]/div[2]', 'class') == 'btn-item-tabs items active':
                    if elementInnerText('//*[@id="pop"]/div/div[1]') != 'アイテム使用確認':
                        click(
                            '//*[@id="prt-target-list"]/div[2]/img',
                            'btn'
                        )
                    else:
                        if elementAttribute('//*[@id="pop"]/div/div[3]/div', 'class') == 'btn-usual-close':
                            js('window.location.href = %s%s%s' % ('"', var.stageHash, '"'))
                            if js('return window.location.hash.split("/")[1]') == 'supporter':
                                var.switch = False
                                break
                        else:
                            ap = int(apNum) - 1
                            if ap != 0:
                                for i in range(ap):
                                    port.keyDown('//*[@id="pop"]/div/div[2]/div/div/div[4]/div[2]/select')
                                click(
                                        '//*[@id="pop"]/div/div[3]/div[2]',
                                        'btn'
                                    )
                            else:
                                click(
                                        '//*[@id="pop"]/div/div[3]/div[2]',
                                        'btn'
                                    )
                else:
                    click(
                        '//*[@id="wrapper"]/div[3]/div[2]/div[2]/div[1]/div[1]/div[2]',
                        'btn'
                    )

def useSupporter():
    if js('retun window.parent.stage.pJsnData.supporter["recast"]') != '0':
        port.click(
            '//*[@id="wrapper"]/div[3]/div[2]/div[9]/div[11]/div[6]',
            'btn'
        )
        