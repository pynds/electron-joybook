import json
import re
import subprocess
import sys
import threading
import time

from requests_oauthlib import OAuth1Session

import port
import var

js = port.javaScript

raidCodeClear = {}

# CK = var.consumerKey
# CS = var.consumerSecret
# AT = var.accessToken
# AS = var.accessTokenSecret

CK = 'CLsGfjgdxDssOvAgzSyQKJZi7'
CS = 'qgofgVzSci7x8ZMxgMTzAeXyePfXGJTXqmXtnHFhMtrEdXsWIE'
AT = '3242022770-0MaWS7QUVdVjgMMoY8eESVIIbcryc4Gr8YdusOI'
AS = 'lwVdGNsn9MmuJwCtx1DFCedbx8TQOrw2LnAV01Fp8cUjP'

FILTER_URL = 'https://stream.twitter.com/1.1/statuses/filter.json'

def parse(string):
    pattern = r'[0-9A-F]{8}\s:参戦ID'
    matchOB = re.findall(pattern, string)
    if matchOB:
        return matchOB[-1][0:8]
    else:
        return None

def getBtId(level, name):
    var.firstRaid = False
    oauth_session = OAuth1Session(CK, CS, AT, AS)
    oauth_session.proxies = {'http':'127.0.0.1:1080', 'https':'127.0.0.1:1080'}
    params = {'track': 'Lv%s %s' % (level, name)}
    req = oauth_session.post(FILTER_URL, params=params, stream=True)
    for line in req.iter_lines():
        line_decode = line.decode('utf-8')
        if line_decode != '':   # if not empty
            tweet = json.loads(line_decode)
            if tweet.get('source') == '<a href="http://granbluefantasy.jp/" rel="nofollow">グランブルー ファンタジー</a>':
                var.raidId = parse(tweet.get('text'))
                print(var.raidId)

def goToRaid():
    var.result = None
    viraApi = port.isEnabledByName('raid')
    while viraApi != None:
        raidCode = var.raidId
        apiOpen = js('return document.title')
        if apiOpen != 'DMCAmate API':
            port.chrome.switch_to.frame('raid')
            print('switch')
        else:
            combatState = js('return tryRefreshCombatState')
            requestId = js('return nextRequestId')
            if combatState != {}:
                js('window.pendingRequests = {};window.nextRequestId = 1;window.msg = null;\
                    window.onRaidLoad = function() {window.addEventListener("message", onRaidMessage, false);window.setInterval(tryRefreshCombatState, 1000);};\
                    window.onRaidMessage = function(evt) {var callback = pendingRequests[evt.data.id];if (!callback)return;callback(evt.data.result);};\
                    window.sendRaidApiRequest = function(request, callback) {var id = nextRequestId++;request.id = id;pendingRequests[id] = callback;postMessage(request, "*");};\
                    window.tryRefreshCombatState = function() {sendRaidApiRequest({ type: "getCombatState" });};\
                    onRaidLoad()\
                ')
            elif requestId != None and requestId > 1 and raidCode != None:
                try:
                    if raidCodeClear[raidCode] != 'complete' and raidCodeClear[raidCode] != 'battleEnd' and raidCodeClear[raidCode] != 'notBattle':
                        join = True
                    else:
                        join = False
                except:
                    join = True
                if join != False:
                    js('sendRaidApiRequest({ type: "tryJoinRaid", raidCode: "%s" }, function(result) {msg = result;});' % (raidCode))
                    while js('return msg') == None:
                        time.sleep(0.1)
                    else:
                        result = js('return msg')
                        if result == 'ok':
                            raidCodeClear[raidCode] = 'complete'
                            var.raidJoin = True
                            break
                        elif result == 'refill required':
                            raidCodeClear[raidCode] = 'notBp'
                            var.eatBp = True
                            break
                        elif result == 'popup: 終了しているマルチバトルです':
                            raidCodeClear[raidCode] = 'battleEnd'
                            var.raidJoin = False
                            break
                        elif result == 'popup: 入力したIDに該当するバトルが<br>見つかりませんでした':
                            raidCodeClear[raidCode] = 'notBattle'
                            var.raidJoin = False
                            break
                        else:
                            print(result)
    else:
        js('iframe = document.createElement("iframe"); iframe.name = "raid"; iframe.src = "chrome-extension://fgpokpknehglcioijejfeebigdnbnokj/content/api.html"; document.body.appendChild(iframe);')

def raidServer():
    raidServer = threading.Thread(target=getBtId, args=(var.raidNameList[0],var.raidNameList[1]))
    raidServer.start()
