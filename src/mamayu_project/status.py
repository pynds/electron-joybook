import port
import job
import fire
chrome = port.chrome
js = port.javaScript

def bossStatus(num, status):
    isStatus = {}
    bossNum = js('return window.stage.gEnemyStatus.length')
    for i in range(bossNum):
        isStatus[i] = {}
        isStatus[i]['alive'] = int(js('return window.parent.stage.gGameStatus.boss.param['+ str(i) +'].alive'))
        isStatus[i]['hpMax'] = int(js('return window.parent.stage.gGameStatus.boss.param['+ str(i) +'].hpmax'))
        isStatus[i]['hp'] = int(js('return window.parent.stage.gGameStatus.boss.param['+ str(i) +'].hp'))
        isStatus[i]['hpPercent'] = int(isStatus[i]['hp']) / int(isStatus[i]['hpMax']) * 100
        isStatus[i]['recastMax'] = int(js('return window.parent.stage.gGameStatus.boss.param['+ str(i) +'].recastmax'))
        isStatus[i]['recast'] = int(js('return window.parent.stage.gGameStatus.boss.param['+ str(i) +'].recast'))
        isStatus[i]['mode'] = int(js('return window.parent.stage.gGameStatus.bossmode.looks.mode['+ str(i) +']'))
    return isStatus[num][status]

def memberStatus(num, status):
    isStatus = {}
    memberNum = js('return window.parent.stage.gGameStatus.player.param.length')
    for i in range(memberNum):
        isStatus[i] = {}
        isStatus[i]['pid'] = js('return window.parent.stage.gGameStatus.player.param['+ str(i) +'].pid')
        isStatus[i]['hp'] = int(js('return window.parent.stage.gGameStatus.player.param['+ str(i) +'].hp'))
        isStatus[i]['hpMax'] = int(js('return window.parent.stage.gGameStatus.player.param['+ str(i) +'].hpmax'))
        isStatus[i]['hpPercent'] = int(isStatus[i]['hp']) / int(isStatus[i]['hpMax']) * 100
        isStatus[i]['recast'] = int(js('return window.parent.stage.gGameStatus.player.param['+ str(i) +'].recast'))
        buffNum = js('return window.stage.gGameStatus.player.param['+ str(i) +'].condition.buff.length')
        debuffNum = js('return window.stage.gGameStatus.player.param['+ str(i) +'].condition.debuff.length')
        if buffNum != None:
            isStatus[i]['buff'] = {}
            for x in range(buffNum):
                isStatus[i]['buff'][x] = js('return window.stage.gGameStatus.player.param['+ str(i) +'].condition.buff['+ str(x) +']["status"]')
        if debuffNum != None:
            isStatus[i]['debuff'] = {}
            for y in range(buffNum):
                isStatus[i]['debuff'][y] = js('return window.stage.gGameStatus.player.param['+ str(i) +'].condition.debuff['+ str(y) +']["status"]')
    return isStatus[num][status]

def buff(this, buffId):
    buff = memberStatus(this, 'buff')
    buffNum = len(buff)
    for i in range(buffNum):
        if buff[i] == buffId:
            return True