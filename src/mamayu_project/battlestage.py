import var
import port
import supporter
import status
import character
import threading
chrome = port.chrome
js = port.javaScript
elementAttribute = port.elementAttribute
elementInnerText = port.elementInnerText
elementStatement = port.elementStatement
click = port.click
goto = port.goto
def battle(stageHash):
    if elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]', 'class') == 'btn-attack-start display-on':
        var.switch = False
        if var.angelHaloHl != 'y':
            if var.ai == 'y':
                menberNum = js('return window.stage.gGameStatus.player.param.length')
                if var.jobAi == 'y':
                    var.point = 0
                    character.characters[status.memberStatus[0]['pid']]()
                else:
                    for i in range(menberNum):
                        var.point = i
                        character.characters[status.memberStatus[i]['pid']]()
                supporter.useSupporter()
                click(
                    '//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]',
                    'btn'
                )
                    
            else:
                click(
                    '//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]',
                    'btn'
                )
        else:
            bossId = js('return parent.stage.gGameStatus.boss.param[0].cjs')
            if bossId != "enemy_8300063":
                click(
                        '//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]',
                        'btn'
                    )
            else:
                hlID = js('return parent.stage.gGameStatus.boss.param[0].enemy_id')
                if hlID != '6005070':
                    print('会变身的')
                    exit()
                else:
                    if elementInnerText('//*[@id="pop"]/div/div[1]') != '撤退確認':
                        if elementInnerText('//*[@id="pop"]/div/div[1]') != 'バトルメニュー':
                            click(
                                '//*[@id="wrapper"]/div[3]/div[2]/div[1]/div[1]/div[6]',
                                'btn'
                            )
                        else:
                            click(
                                '//*[@id="pop"]/div/div[2]/div/div/div[8]/div[2]',
                                'btn'
                            )
                    else:
                        click(
                            '//*[@id="pop"]/div/div[3]/div[2]',
                            'btn'
                        )
                        
    elif elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]', 'class') == 'btn-attack-start display-off' and elementAttribute('//*[@id="loading"]/div', 'style') != 'display: block;':
            if js('return document.getElementsByClassName("prt-battle-num")[0].getElementsByClassName("txt-info-num")[0].children[2].className.substr(8,9)') != None:
                if int(js('return document.getElementsByClassName("prt-battle-num")[0].getElementsByClassName("txt-info-num")[0].children[2].className.substr(8,9)')) > 1:
                    js('window.location.href = %s%s%s' % ('"', stageHash, '"'))
                    port.time.sleep(2)
                else:
                    chrome.refresh()
            else:
                chrome.refresh()

def reQuest(stageHash):
    if var.angelHaloHl != 'y':
        if port.isElement('//*[@id="cnt-result"]/div[1]/div[1]/div[1]') != None:
            js('window.location.href = %s%s%s' % ('"', stageHash, '"'))
    else:
        if port.isElement('//*[@id="cnt-result"]/div[1]/div[1]/div[1]') != None:
            js('window.location.href = "#quest/extra"')
        elif port.isElement('//*[@id="tab-normal-quest"]') != None:
            questList = js('return document.getElementsByClassName("prt-extra")[0].getElementsByClassName("prt-module")[0].children.length')
            isHl = False
            for i in range(questList):
                if elementAttribute('//*[@id="cnt-normal-quest"]/div/div/div[%s]/div/div[2]' % (i), 'data-id') == '100':
                    isHl = True
                    break
            if isHl == True:
                js('window.location.href = "#quest/supporter/510051/5"')
            else:
                js('window.location.href = %s%s%s' % ('"', stageHash, '"'))

def raidBattle():
    if elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]', 'class') == 'btn-attack-start display-on':
        if var.battleType != '0':
            menberNum = js('return window.stage.gGameStatus.player.param.length')
            for i in range(menberNum):
                var.point = i
                character.characters[status.memberStatus[i]['pid']]()
            supporter.useSupporter()
            click(
                '//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]',
                'btn'
            )
        else:
            click(
                '//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]',
                'btn'
            )
    elif elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[7]/div[4]', 'class') == 'btn-attack-start display-off' and elementAttribute('//*[@id="loading"]/div', 'style') != 'display: block;':
        if var.battleType != '0':
            chrome.refresh()
        else:
            var.raidJoin = False