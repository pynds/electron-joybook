const pendingRequests = {};
let nextRequestId = 1;
const msg = null;
const code = null;

function onLoad() {
	window.addEventListener('message', onMessage, false);
	window.setInterval(tryRefreshCombatState, 1000);
}

function onMessage(evt) {
	const callback = pendingRequests[evt.data.id];
	if (!callback) return;
	callback(evt.data.result);
}

function sendApiRequest(request, callback) {
	const id = nextRequestId++;
	request.id = id;
	pendingRequests[id] = callback;
	postMessage(request, '*');
}

function tryRefreshCombatState() {
	sendApiRequest({ type: 'getCombatState' });
}

window.addEventListener('DOMContentLoaded', onLoad, false);

// sendApiRequest({ type: "tryJoinRaid", raidCode: 'ABCDEFG' }, function(result) { msg = result; });
