import os
import sys
import var
import signal

# Setting LANG=en_US.UTF-8 before executing my script
os.environ["LANG"] = "en_US.UTF-8"

def quit(signum, frame):
    print('You choose to stop me.')
    sys.exit()

signal.signal(signal.SIGINT, quit)

path = 'setting/'
items = os.listdir(path)
fast = None
for i in range(len(items)):
    if items[i].endswith('.my'):
        var.load = input('是否读取设置? Y/N:')
        var.load = var.load.lower()
        fast = True
        break

if fast != True:
    print('使用说明:')
    print('1.启动前关闭chrome')
    print('2.为chrome快捷方式添加启动参数"--remote-debugging-port=9222"(不会的自己退)')
    print('3.通过设置有启动的参数的chrome快捷方式打开游戏并将游戏分辨率改为一倍')
    print('4.正式启动前打开目录下"chromedriver.exe"')
    print('5.关卡地址为召唤页面“game.granbluefantasy.jp/”后的地址(包括#号)')
    print('6.例如:"http://game.granbluefantasy.jp/#quest/supporter/510031/5"(星号)')
    print('则关卡地址为"#quest/supporter/510031/5"(别带上双引号cnm)')
    print('7.手动进入召唤界面并启动脚本')
    print('8.牢底坐穿')
    os.system('pause')

if var.load == 'y':
    fileName = input('输一个你保存的文件名:')
    options = open('%s%s%s' % (path, fileName, '.my'), 'r')
    options = options.readlines()
    var.stageType = options[0].strip('\n')
    if var.stageType == '0':
        var.stageHash = options[1].strip('\n')
        var.apNum = options[2].strip('\n')
        var.angelHaloHl = options[3].strip('\n')
        var.ai = options[4].strip('\n')
        var.order[0] = options[5].strip('\n')
        var.order[1] = options[6].strip('\n')
        var.order[2] = options[7].strip('\n')
        var.order[3] = options[8].strip('\n')
        var.jobAi = options[9].strip('\n')
        var.ptType = options[10].strip('\n')
        var.sup1 = options[11].strip('\n')
        var.sup2 = options[12].strip('\n')
    elif var.stageType == '1':
        var.coopStage = options[1].strip('\n')
        var.apNum = options[2].strip('\n')
        var.coopCharNum = options[3].strip('\n')
        var.coopSkillNum = options[4].strip('\n')
        var.coopItem = options[5].strip('\n')
        var.coopItemList = var.coopItem.strip(',').split(',')
    elif var.stageType == '2':
        var.consumerKey = options[1].strip('\n')
        var.consumerSecret = options[2].strip('\n')
        var.accessToken = options[3].strip('\n')
        var.accessTokenSecret = options[4].strip('\n')
        var.raidName = options[5].strip('\n')
        var.raidNameList = var.raidName.strip(',').split(',')
        var.bpNum = var.coopItem.strip(',').split(',')
        var.battleType = options[6].strip('\n')
        var.ptType = options[7].strip('\n')
        var.sup1 = options[8].strip('\n')
        var.sup2 = options[9].strip('\n')
    else:
        print('注意点')
        exit()

else:
    print('周回(0)/共斗(1)/舔婊(2)')
    var.stageType = input(':')
    if var.stageType == '0':
        var.stageHash = input('输一个你需要周回的关卡地址:')
        var.apNum = input('每次AP不足时你要吔几多半红啊?:')
        if var.stageHash == '#quest/supporter/510031/5':
            var.angelHaloHl = input('检测到周回副本为星号,开唔开HL检测? Y/N:')
            var.angelHaloHl = var.angelHaloHl.lower()
        var.ai = input('是否开启AI Y/N:')
        var.ai = var.ai.lower()
        if var.ai == 'y':
            print('由于主角技能组合的不固定性,需要为其设定一下技能启动顺序')
            print('哈士奇(0)/斯巴达(1)/兔子(2)/魔法少女(3)/义贼(4)/混沌(5)/拳皇(6)/猎犬(7)/C4SS(8)/天女(9)')
            job = input('选一个你作为主角的职业:')
            if job == '0':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            elif job == '1':
                print('MAIN:即奥(66) ZP:大破甲(4594)/')
            elif job == '2':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            elif job == '3':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            elif job == '4':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            elif job == '5':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            elif job == '6':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            elif job == '7':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            elif job == '8':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            elif job == '9':
                print('MAIN:即奥(66) ZP:大破甲(8000)/攻大Ⅳ(8001)/')
            else:
                print('别瞎鸡巴输')
            print('EX:掺雾(8002)')
            var.order[0] = input('输入第一顺序被发动的技能ID:')
            var.order[1] = input('输入第二顺序被发动的技能ID:')
            var.order[2] = input('输入第三顺序被发动的技能ID:')
            var.order[3] = input('输入第四顺序被发动的技能ID:')
            var.jobAi = input('是否仅打开主角AI(用于需要上db的关卡) Y/N')
            var.jobAi = var.jobAi.lower()
        print('选择召唤属性')
        print('自由(0)/火(1)/水(2)/土(3)/风(4)/光(5)/暗(6)')
        var.ptType = input(':')
        if var.ptType == '0':
            print('辉夜(2040114000)/兔子(2030026000)')
        elif var.ptType == '1':
            print('湿婆(2040185000)')
        else:
            print('还没做cnm')
        var.sup1 = input('输入优先蹭的召唤:')
        var.sup2 = input('输入次要蹭的召唤:')
        varSave = input('是否存储设置? Y/N:')
        varSave = varSave.lower()
        if varSave == 'y':
            fileName = input('起翻个名啦:')
            while os.path.exists('%s.my' % (fileName)) != False or os.path.getsize('%s.my' % (fileName)) != 0:
                files = open('%s%s%s' % (path, fileName, '.my'), 'w')
                files.write(var.stageType + '\n')
                files.write(var.stageHash + '\n')
                files.write(var.apNum + '\n')
                if var.angelHaloHl != 'y':
                    files.write('none' + '\n')
                else:
                    files.write(var.angelHaloHl + '\n')
                files.write(var.ai + '\n')
                if var.ai == 'y':
                    files.write(var.order[0] + '\n')
                    files.write(var.order[1] + '\n')
                    files.write(var.order[2] + '\n')
                    files.write(var.order[3] + '\n')
                    files.write(var.jobAi + '\n')
                else:
                    files.write('none' + '\n')
                    files.write('none' + '\n')
                    files.write('none' + '\n')
                    files.write('none' + '\n')
                    files.write('none' + '\n')
                files.write(var.ptType + '\n')
                files.write(var.sup1 + '\n')
                files.write(var.sup2 + '\n')
    elif var.stageType == '1':
        var.coopStage = input('踢罐(0)/木桩(1):')
        coopKillType = input('剑圣(0)/其他技能(1):')
        if coopKillType != '0':
            var.coopCharNum = input('第几个角色?:')
            var.coopSkillNum = input('第几个技能?:')
        var.apNum = input('每次AP不足时你要吔几多半红啊?:')
        print ('选择要开启的共斗buff(以英逗号隔开):')
        var.coopItem = input('Rank(0)/Exp(1)/掉宝(2):')
        var.coopItemList = var.coopItem.strip(',').split(',')
        varSave = input('是否存储设置? Y/N:')
        varSave = varSave.lower()
        if varSave == 'y':
            fileName = input('起翻个名啦:')
            while os.path.exists('%s.my' % (fileName)) != False or os.path.getsize('%s.my' % (fileName)) != 0:
                files = open('%s%s%s' % (path, fileName, '.my'), 'w')
                files.write(var.stageType + '\n')
                files.write(var.coopStage + '\n')
                files.write(var.apNum + '\n')
                files.write(var.coopCharNum + '\n')
                files.write(var.coopSkillNum + '\n')
                files.write(var.coopItem + '\n')
    elif var.stageType == '2':
        var.consumerKey = input('consumerKey:')
        var.consumerSecret = input('consumerSecret:')
        var.accessToken = input('accessToken:')
        var.accessTokenSecret = input('accessTokenSecret:')
        print('婊名格式为(level,婊全名),例(75,シュヴァリエ・マグナ)')
        var.raidName = input('输入要舔的婊名:')
        var.raidNameList = var.raidName.strip(',').split(',')
        var.bpNum = input('每次BP不足时你要吔几多粒豆啊?:')
        var.battleType = input('一拳定系用力打? 一拳(0)/用力打(1):')
        if var.battleType != 1:
            var.raidBattleType = input('是否开启技能一拳? Y/N:')
            var.raidBattleType = raidBattleType.lower()
            if var.raidBattleType == 'y':
                var.coopCharNum = input('第几个角色?:')
                var.coopSkillNum = input('第几个技能?:')
        print('选择召唤属性')
        print('自由(0)/火(1)/水(2)/土(3)/风(4)/光(5)/暗(6)')
        var.ptType = input(':')
        if var.ptType == '0':
            print('辉夜(2040114000)/兔子(2030026000)')
        elif var.ptType == '1':
            print('湿婆(2040185000)')
        else:
            print('还没做cnm')
        var.sup1 = input('输入优先蹭的召唤:')
        var.sup2 = input('输入次要蹭的召唤:')
        varSave = input('是否存储设置? Y/N:')
        varSave = varSave.lower()
        if varSave == 'y':
            fileName = input('起翻个名啦:')
            while os.path.exists('%s.my' % (fileName)) != False or os.path.getsize('%s.my' % (fileName)) != 0:
                files = open('%s%s%s' % (path, fileName, '.my'), 'w')
                files.write(var.consumerKey + '\n')
                files.write(var.consumerSecret + '\n')
                files.write(var.accessToken + '\n')
                files.write(var.accessTokenSecret + '\n')
                files.write(var.raidName + '\n')
                files.write(var.bpNum + '\n')
                files.write(var.battleType + '\n')
                files.write(var.ptType + '\n')
                files.write(var.sup1 + '\n')
                files.write(var.sup2 + '\n')
    else:
        print('别捣乱')
        exit()

import repeat
