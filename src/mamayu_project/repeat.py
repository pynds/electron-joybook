import var
import port
import supporter
import battlestage
import status
import character
import threading
import os
import coop
import raid
from multiprocessing import Process
chrome = port.chrome
js = port.javaScript
elementAttribute = port.elementAttribute
elementInnerText = port.elementInnerText
elementStatement = port.elementStatement
click = port.click
goto = port.goto
isGbf = js('return window.location.host')
idList = (
    5580925,
    6220695,
    5698100,
    7731400,
    15478563,
    7503110,
    11422459,
    4529403,
    12327714,
    7769683,
    7668658,
    15383043,
    10103608,
    9087642,
    9194334,
    15591628
)
while isGbf == 'game.granbluefantasy.jp':
    port.chrome.switch_to.default_content()
    thisId = js('return parent.Game.userId')
    if thisId in idList:
        if var.stageType == '0':
            urlHash = js('return window.location.hash.split("/")')
            urlHashNum = len(urlHash)
            if urlHash[0] == '#raid':
                battlestage.battle(var.stageHash)
            elif elementInnerText('//*[@id="wrapper"]/header/div[3]') == 'クエストリスト':
                goto(var.stageHash)
            elif urlHashNum >= 2:
                if urlHash[0] == '#result' or urlHash[1] == 'extra':
                    battlestage.reQuest(var.stageHash)
                elif urlHash[1] == 'supporter':
                    supporter.typeSelect(var.ptType, var.apNum)
        elif var.stageType == '1':
            coop.coopStage(var.coopStage, var.apNum)
        elif var.stageType == '2':
            if var.firstRaid != False:
                raid.raidServer()
            elif var.raidJoin != True:
                raid.goToRaid()
            urlHash = js('return window.location.hash.split("/")')
            urlHashNum = len(urlHash)
            if urlHash[0] == '#raid_multi':
                battlestage.raidBattle()
            elif urlHashNum >= 2:
                if urlHash[0] == '#result':
                    battlestage.reQuest(var.stageHash)
                elif urlHash[1] == 'supporter_raid':
                    supporter.typeSelect(var.ptType, var.bpNum)
    else:
        print('你网络有点屎')
