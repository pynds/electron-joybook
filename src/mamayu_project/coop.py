import sys
from multiprocessing import process
import port
import var
import time
chrome = port.chrome
js = port.javaScript
elementAttribute = port.elementAttribute
elementInnerText = port.elementInnerText
elementStatement = port.elementStatement
elementByClassName = port.elementByClassName
click = port.click
goto = port.goto

skillNum = {
    '1' : 'Q',
    '2' : 'W',
    '3' : 'E',
    '4' : 'R'
}

stages = {
    '0' : '"#coopraid/room/entry/601011"',
    '1' : '"#coopraid/room/entry/601431"'
}

itemNames = {
    '0' : '鍛錬の巻',
    '1' : '修練の巻',
    '2' : '風見鶏の羽'
 }

def coopBuffs():
    itemBox = None
    if elementAttribute('//*[@id="item-list-tabs"]', 'class') != 'btn-tabs active':
        port.moveToElement('//*[@id="item-list-tabs"]')
        time.sleep(1)
        click(
            '//*[@id="item-list-tabs"]',
            'btn'
        )
    else:
        itemBox = js('return document.getElementsByClassName("prt-special-list")[0].children.length')
    if itemBox != None:
        for i in range(len(var.coopItemList)):
            itemName = itemNames[var.coopItemList[i]]
            for x in range(int(itemBox)):
                isItemName = elementInnerText('//*[@id="cnt-item-list"]/div/div[2]/div[2]/div[2]/div[%s]/div[2]/div[1]/div[1]' % (x+1))
                if isItemName == itemName:
                    while elementInnerText('//*[@id="cnt-item-list"]/div/div[2]/div[2]/div[2]/div[%s]/div[2]/div[2]/div[2]' % (x+1)) != '効果LvがMAXです':
                        port.moveToElement('//*[@id="cnt-item-list"]/div/div[2]/div[2]/div[2]/div[%s]'  % (x+1))
                        time.sleep(1)
                        while elementInnerText('//*[@id="cnt-item-list"]/div/div[2]/div[2]/div[2]/div[%s]/div[2]/div[2]/div[2]' % (x+1)) != '効果LvがMAXです':
                            if elementInnerText('//*[@id="pop"]/div/div[1]') != 'アイテム使用確認':
                                click(
                                    '//*[@id="cnt-item-list"]/div/div[2]/div[2]/div[2]/div[%s]/div[2]/div[3]/div' % (x+1),
                                    'btn'
                                )
                            else:
                                click(
                                    '//*[@id="pop"]/div/div[3]/div[2]',
                                    'btn'
                                )
        port.moveToElement('//*[@id="wrapper"]/header/div[3]')
        time.sleep(1)
        var.roomId = js('return window.location.hash')

def coopStage(stage, apNum):
    isCoop = elementInnerText('//*[@id="wrapper"]/header/div[3]')
    if isCoop == 'ルーム':
        var.isRoom = True
    elif isCoop == '共闘':
        var.isRoom = False
        var.roomId = None

    if var.isRoom != True:
        isRecruitTitle = elementInnerText('//*[@id="wrapper"]/div[3]/div[2]/div[1]/div[1]')
        isLoading = elementAttribute('//*[@id="ready"]', 'style')
        if isRecruitTitle != '仲間を集める':
            js('window.location.href = %s' % (stages[stage]))
        else:
            isRecruit = elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[1]/div[3]/div[9]/div', 'class')
            if isRecruit == 'btn-post-guild checked':
                click(
                    '//*[@id="wrapper"]/div[3]/div[2]/div[1]/div[3]/div[9]/div',
                    'btn'
                )
            else:
                click(
                    '//*[@id="wrapper"]/div[3]/div[2]/div[1]/div[5]/div',
                    'btn'
                )
                var.isRoom = True
    else:
        isRoomTitle = elementInnerText('//*[@id="wrapper"]/header/div[3]')
        if isRoomTitle == 'ルーム':
            if port.elementInnerText('//*[@id="pop"]/div/div[1]') == 'APが足りません':
                ap = int(apNum) - 1
                if ap != 0:
                    for i in range(ap):
                        port.keyDown('//*[@id="pop"]/div/div[2]/div/div[2]/div[2]/div[4]/select')
                    click(
                            '//*[@id="pop"]/div/div[2]/div/div[2]/div[2]/div[5]/div',
                            'btn'
                        )
                else:
                    click(
                            '//*[@id="pop"]/div/div[2]/div/div[2]/div[2]/div[5]/div',
                            'btn'
                        )
            elif var.roomId == None:
                coopBuffs()
            else:
                questReady = elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[4]/div/div[1]/div/div[3]/div[2]/div', 'class')
                if questReady == 'btn-make-ready-large not-ready':
                    click(
                        '//*[@id="wrapper"]/div[3]/div[2]/div[4]/div/div[1]/div/div[3]/div[2]/div',
                        'btn'
                    )
                else:
                    coopStart = elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[4]/div/div[1]/div/div[2]/div[1]/div[3]', 'class')
                    if coopStart == 'btn-quest-start multi se-quest-start onm-tc-gbf':
                        click(
                            '//*[@id="wrapper"]/div[3]/div[2]/div[4]/div/div[1]/div/div[2]/div[1]/div[3]',
                            'btn'
                        )
        else:
            coopSupporter = elementInnerText('//*[@id="wrapper"]/div[3]/div[3]/div[2]/div/div[1]')
            if coopSupporter == 'サポーター召喚石無しでクエストに挑戦します':
                click(
                    '//*[@id="wrapper"]/div[3]/div[3]/div[3]/div[2]',
                    'btn'
                )

    battleReady = elementAttribute('//*[@id="wrapper"]/div[3]/div[2]/div[16]', 'style')
    while battleReady != 'display: none;' and battleReady != None:
        okDisplay = elementAttribute('//*[@id="wrapper"]/div[3]/div[8]/div[3]/div[2]', 'style')
        if okDisplay == 'display: none;':
            chrome.refresh()
        result = elementAttribute('//*[@id="cnt-result"]/div[1]/div[1]/div[1]', 'class')
        if result == "prt-result-head head-win":
            chrome.back()
            break
        else:
            port.shunGokuSatsu(var.coopCharNum, skillNum[var.coopSkillNum])