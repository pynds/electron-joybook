import sys
import os
import random
import time
from selenium import webdriver
from selenium.webdriver import Remote, ChromeOptions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
import var

option = webdriver.ChromeOptions()
option.binary_location = r"C:\Users\pynds\Desktop\joybook\build\win-unpacked\joybook.exe"
option.debugger_address = '127.0.0.1:9222'
chrome = Remote(command_executor='http://127.0.0.1:9515',desired_capabilities=option.to_capabilities())
handles = chrome.window_handles

# TODO: 窗口标题需要传参进来
for window in handles:
    chrome.switch_to_window(window)
    if chrome.title != 'joybook':
        break

if var.stageType != '0' and var.playNum == '1':
	option2 = webdriver.ChromeOptions()
	option2.debugger_address = '127.0.0.1:9333'
	chrome2 = Remote(command_executor='http://127.0.0.1:9515',desired_capabilities=option2.to_capabilities())

def elementStatement(xpathRoute):
	try:
		chrome.find_element_by_xpath(xpathRoute)
	except:
		return False
	else:
		return True

def elementAttribute(xpathRoute, attribute):
	try:
		return chrome.find_element_by_xpath(xpathRoute).get_attribute(attribute)
	except:
		return None

def elementByClassName(className):
	try:
		chrome.find_element_by_class_name(className)
	except:
		return None
	else:
		return True

def elementInnerText(xpathRoute):
	try:
		return chrome.find_element_by_xpath(xpathRoute).text
	except:
		return None

def click(xpathRoute, btnType):
	try:
		size = chrome.find_element_by_xpath(xpathRoute).size
		if btnType == 'skill':
			randWidth = random.uniform(0, 20)
			randHeight = random.uniform(0, 20)
		elif btnType == 'sup':
			randWidth = random.uniform(0, 250)
			randHeight = random.uniform(0, 40)
		else:
			randWidth = random.uniform(0, size['width'])
			randHeight = random.uniform(0, size['height'])
		loction = chrome.find_element_by_xpath(xpathRoute).location
		if loction['x'] > 0 and loction['y'] > 0:
			ActionChains(chrome).move_to_element_with_offset(chrome.find_element_by_xpath(xpathRoute), randWidth, randHeight).click().perform()
			time.sleep(random.uniform(0.5, 1))
	except:
		return None

def moveToElement(xpathRoute):
	try:
		isElement = chrome.find_element_by_xpath(xpathRoute)
		chrome.execute_script("arguments[0].scrollIntoViewIfNeeded();", isElement)
	except:
		return None

def isElement(xpathRoute):
	try:
		return chrome.find_element_by_xpath(xpathRoute)
	except:
		return None

def isElementByClassName(className):
	try:
		return chrome.find_element_by_class_name(className)
	except:
		return None

def isEnabled(xpathRoute):
	try:
		chrome.find_element_by_xpath(xpathRoute)
	except:
		return None
	else:
		return True

def isEnabledByClassName(className):
	try:
		chrome.find_element_by_class_name(className)
	except:
		return None
	else:
		return True

def isEnabledById(isid):
	try:
		chrome.find_element_by_id(isid)
	except:
		return None
	else:
		return True

def isEnabledByName(isName):
	try:
		chrome.find_element_by_name(isName)
	except:
		return None
	else:
		return True

def javaScript(code):
	try:
		return chrome.execute_script(code)
	except:
		return None

def goto(stage):
	chrome.execute_script('window.location.href = %s' %('"'+stage+'"'))

def keyDown(xpathRoute):
	try:
		chrome.find_element_by_xpath(xpathRoute).send_keys(Keys.DOWN)
	except:
		return None

def keyEnter(xpathRoute):
	chrome.find_element_by_xpath(xpathRoute).send_keys(Keys.ENTER)

def elementByXpath(xpathRoute):
	try:
		return chrome.find_element_by_xpath(xpathRoute)
	except:
		return None

def elementMoveTop(xpathRoute):
	try:
		target = chrome.find_element_by_xpath(xpathRoute)
		chrome.execute_script("arguments[0].scrollIntoView();", target)
	except:
		return None

def keyKickPot(xpathRoute):
	try:
		chrome.find_element_by_xpath(xpathRoute).send_keys(Keys.NUMPAD1,'Q',Keys.SPACE)
	except:
		return None

def shunGokuSatsu(char, skill):
	try:
		chrome.find_element_by_xpath('/html/body').send_keys(char, skill, Keys.SPACE)
	except:
		return None
