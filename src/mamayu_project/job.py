import var
import status
import port
import skill
useSkill = skill.useSkill
chrome = port.chrome

#mainSkill
def fullArsenal():
	if status.memberStatus(var.point, 'recast') < 100:
		useSkill('66')

#lbSkill
def defenseBreach():
	useSkill('8000')

def rage():
	if status.memberStatus(var.point, 'recast') >= 30:
		useSkill('8001')

#exSkill
def miserableMist():
	useSkill('8002')

skillList = {
	'66' : fullArsenal,
	'8000' : defenseBreach,
	'8001' : rage,
	'8002' : miserableMist
}

def jobMain():
    for i in range(len(var.order)):
        skillList[var.order[i]]()