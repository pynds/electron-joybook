import port
chrome = port.chrome

def skillAttribute(skillId):
	return port.elementAttribute('//div[@class="quick-panel prt-ability-list"]//*[@ability-id='+ skillId +']/..', 'class')

def skillDiv(skillId):
	return '//div[@class="quick-panel prt-ability-list"]//*[@ability-id='+ skillId +']'

def useSkill(skillId):
	while skillAttribute(skillId) == 'lis-ability btn-ability-available quick-button':
		if port.elementLocation(skillDiv(skillId)) != False:
			port.click(
				skillDiv(skillId),
				'skill'
			)
		#if skillAttribute(skillId) == 'lis-ability btn-ability-unavailable quick-button':
