import var
import port
import status
import skill

#characters
def anila():
    skill.useSkill('3457')
    skill.useSkill('781')
    skill.useSkill('2099')
    if status.bossStatus(var.point, 'recast') != status.bossStatus(var.point, 'recastMax') and status.bossStatus(var.point, 'mode') != 3:
        skill.useSkill('1205')

def percival():
    skill.useSkill('6188')
    if status.buff(var.point, '4011') == True:
        skill.useSkill('4083')
    if status.bossStatus(var.point, 'recast') != status.bossStatus(var.point, 'recastMax') and status.bossStatus(var.point, 'mode') != 3:
        skill.useSkill('1961')

def yuel():
    skill.useSkill('931')
    if status.memberStatus(var.point, 'hpPercent') < 70:
        skill.useSkill('3061')
    skill.useSkill('4029')
    skill.useSkill('2182')