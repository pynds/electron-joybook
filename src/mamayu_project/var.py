#加载所有从joybookUI进程传入的JSON

import json
import sys

def parseJSON(stringifyJSON):
    #因为windows传参要用双引号
    #从UI传过来的JSON也一样是replace过的
    strJSON = stringifyJSON.replace('\'', '"')
    return json.loads(strJSON)

battleSettings = parseJSON(sys.argv[1])

#ctrl + f 正则匹配 正则: battleSettings\. 就能匹配所有这些 随你替换
item = battleSettings.item
start = battleSettings.start
stageHash = battleSettings.stageHash
apNum = battleSettings.apNum
isHl = battleSettings.isHl
angelHaloHl = battleSettings.angelHaloHl
ai = battleSettings.ai
sup1 = battleSettings.sup1
sup2 = battleSettings.sup2
load = battleSettings.load
jobAi = battleSettings.jobAi
ptType = battleSettings.ptType
order = battleSettings.order
point = battleSettings.point
switch = battleSettings.switch
fastId = battleSettings.fastId
oldId = battleSettings.oldId
isId = battleSettings.isId
stageType = battleSettings.stageType
coopStage = battleSettings.coopStage
coopCharNum = battleSettings.coopCharNum
coopSkillNum = battleSettings.coopSkillNum
playNum = battleSettings.playNum
isRoom = battleSettings.isRoom
coopItem = battleSettings.coopItem
coopItemList = battleSettings.coopItemList
roomId = battleSettings.roomId
consumerKey = battleSettings.consumerKey
consumerSecret = battleSettings.consumerSecret
accessToken = battleSettings.accessToken
accessTokenSecret = battleSettings.accessTokenSecret
raidName = battleSettings.raidName
raidNameList = battleSettings.raidNameList
bpNum = battleSettings.bpNum
raidId = battleSettings.raidId
firstRaid = battleSettings.firstRaid
raidJoin = battleSettings.raidJoin
eatBp = battleSettings.eatBp
#raidCodeClear
raidCodeClear = battleSettings.raidCodeClear
battleType = battleSettings.battleType
result = battleSettings.result
raidBattleType = battleSettings.raidBattleType



#初始化
# item = '#item'
# start = True
# stageHash = None
# apNum = None
# isHl = None
# angelHaloHl = None
# ai = None
# sup1 = None
# sup2 = None
# load = None
# isHl = None
# jobAi = None
# ptType = None
# order = {
#     0 : None,
#     1 : None,
#     2 : None,
#     3 : None
# }
# point = None
# switch = False
# isHl = None
# fastId = None
# oldId = None
# isId = None
# stageType = None
# coopStage = None
# coopCharNum = '1'
# coopSkillNum = '1'
# playNum = None
# isRoom = None
# coopItem = None
# coopItemList = None
# roomId = None
# consumerKey = None
# consumerSecret = None
# accessToken = None
# accessTokenSecret = None
# raidName = None
# raidNameList = None
# bpNum = None
# raidId = None
# firstRaid = True
# raidJoin = None
# eatBp = None
# raidCodeClear = {}
# battleType = None
# result = None
# raidBattleType = None
