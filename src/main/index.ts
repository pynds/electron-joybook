import { app, clipboard, dialog, Event, ipcMain } from 'electron';
import Store from 'electron-store';
import { IGBFGameStatus } from '../types';
import GBFWindow from './GBFWindow';
import UIWindow from './UIWindow';

const layout = new Store({
	name: 'applayout',
});

interface IStore {
	[index: string]: Store;
}

const store: IStore = {
	layout,
};

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
	// tslint:disable-next-line
	(global as any).__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\');
}

app.commandLine.appendSwitch('--remote-debugging-port', '9222');
app.commandLine.appendSwitch('--disable-dev-tools');

let gbfWindow: GBFWindow;
let uiWindow: UIWindow;

const gbfLocation = 'http://game.granbluefantasy.jp/';
const joybookUI = process.env.NODE_ENV === 'development' ?
	'http://localhost:9080' :
	`file://${__dirname}/index.html`;

function createWindow(): void {
	/**
	 * Initial window options
	 */
	gbfWindow = new GBFWindow({
		width: layout.get('gbfWindow.size.width', 760),
		height: layout.get('gbfWindow.size.height', 960),
		x: layout.get('gbfWindow.position.x', false),
		y: layout.get('gbfWindow.position.y', false),
		useContentSize: true,
		frame: true,
		resizable: true,
		minimizable: true,
		maximizable: true,
		fullscreenable: true,
		webPreferences: {
			/**
			 * 完整支持node会把require这个全局变量覆盖
			 * 导致GBF窗口使用的并非真正的requirejs，所以关闭完整支持node
			 * @summary 关闭完整支持node使GBF窗口正常加载
			 */
			nodeIntegration: false,
		},
	});

	gbfWindow.loadURL(gbfLocation);

	/**
	 *      gbf       :  transfer  :       ui
	 *      👆              ↑↑            👆
	 *    发送窗口        指令内容       目标窗口
	 * @description 如果第三内容是窗口 第三内容为目标 handler 有参数
	 *
	 *      ui       :  invoke
	 *      👆            ↑↑
	 *   目标窗口       指令内容
	 * @description 如果没有第三内容 第一内容为目标 handler 没有参数
	 * @example ui:invoke 这是由gbf发出的事件 作用于ui上
	 *
	 *     gbf    :     ui     :    reset
	 *     👆           👆            ↑
	 *  目标窗口      目标窗口       指令内容
	 * @description 如果第三内容不是窗口 handler 参数随意
	 *
	 * @summary 自定义指令含义规范
	 */

	gbfWindow.on('ui:invoke' as any, (): void => {
		uiWindow.show();
	});

	gbfWindow.on('gbf:transfer:ui' as any, (data): void => {
		uiWindow.webContents.send('gbf:transfer:ui', data);
	});
	gbfWindow.on('gbf:gameStatus:ui' as any, (error: string, data?: IGBFGameStatus): void => {
		uiWindow.webContents.send('gbf:gameStatus:ui', error, data);
	});

	gbfWindow.on('gbf:canForward:ui' as any, (can): void => uiWindow.webContents.send('gbf:canForward:ui', can));
	gbfWindow.on('gbf:canBackward:ui' as any, (can): void => uiWindow.webContents.send('gbf:canBackward:ui', can));

	/**
	 * 模拟parent行为
	 * 如果主窗口关闭，UI也关闭
	 * @summary 模拟parent行为
	 */
	gbfWindow.on('close', (): void => {
		gbfWindow.removeAllListeners();
		gbfWindow = null;
		if (uiWindow === null) return;
		uiWindow.close();
	});

	/**
	 * 这里是UiWindow
	 */
	uiWindow = new UIWindow({
		width: layout.get('uiWindow.size.width', 800),
		height: layout.get('uiWindow.size.height', 600),
		x: layout.get('uiWindow.position.x', false),
		y: layout.get('uiWindow.position.y', false),
		useContentSize: true,
		frame: false,
		minWidth: 480,
	});

	uiWindow.loadURL(joybookUI);
	uiWindow.on('close', (ev): void => {
		uiWindow.removeAllListeners();
		uiWindow = null;
	});

	/**
	 * 当点击重置窗体记忆时触发
	 */
	ipcMain.on('ui:gbf:reset' as any, (ev: Event, storeName: string): void => {
		store[storeName].clear();
		dialog.showMessageBox({
			type: 'info',
			title: '窗体记忆已重置',
			message: '如果你不再改变窗体位置与大小\n下次开启joybook就恢复默认位置和大小',
			buttons: ['了解了'],
		});
	});
}

app.on('ready', () => {
	if (process.argv.length > 1 && process.env.NODE_ENV !== 'development') {
		dialog.showErrorBox('Illegal argv', `${process.argv.toString().replace(',', '\n')}`);
		process.exit(1);
	}
	createWindow();
});

app.on('window-all-closed', () => {
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	if (gbfWindow === null || uiWindow === null) {
		createWindow();
	}
});

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
