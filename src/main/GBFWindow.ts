import { BrowserWindowConstructorOptions, Event, ipcMain } from 'electron';
import Store from 'electron-store';
import { IGBFGameStatus } from '../types';
import Window from './Window';

const layout = new Store({
	name: 'applayout',
});

export default class GBFWindow extends Window {
	constructor(options: BrowserWindowConstructorOptions) {
		super(options);

		this.initEventListener();
		this.initUiAttach();
		this.initGenericListener();
	}

	public onResize(): void {
		const size = {
			width: this.getSize()[0],
			height: this.getSize()[1],
		};
		layout.set('gbfWindow.size', size);
	}

	public onMove(): void {
		const position = {
			x: this.getPosition()[0],
			y: this.getPosition()[1],
		};
		layout.set('gbfWindow.position', position);
	}

	public onMaximize(): void {}
	public onUnmaximize(): void {}

	private initGenericListener() {
		enum gbfLang {
			en = 'en',
			jp = 'jp',
		}
		// 当点击开始时检测语言环境。
		// TODO: 还没有适配
		ipcMain.on('gbf:language', () => {
			/**
			 * 检测是否为英文环境
			 */
			this.webContents.executeJavaScript(`
				new Promise((resolve, reject) => {
					try {
						if (window.Game.lang === 'en') {
							return resolve('en');
						} else {
							return resolve('jp');
						}
					} catch (e) {
						return reject(e);
					}
				})
			`, true)
			.then((lang: gbfLang) => this.emit('gbf:language:ui', null, lang))
			.catch((error) => this.emit('gbf:language:ui', `${error}`));
		});
		ipcMain.on('gbf:refresh', () => {
			this.reload();
		});
		ipcMain.on('gbf:forward', () => {
			this.webContents.goForward();
		});
		ipcMain.on('gbf:backward', () => {
			this.webContents.goBack();
		});
		ipcMain.on('gbf:gameStatus', () => {
			this.webContents.executeJavaScript(`
				new Promise((resolve, reject) => {
					if (window.stage) {
						return resolve(window.stage.gGameStatus);
					}
					return reject();
				})
			`, true)
			.then((status: IGBFGameStatus) => this.emit('gbf:gameStatus:ui', null, status))
			.catch((error) => this.emit('gbf:gameStatus:ui', `${error}`));
		});
	}

	private initUiAttachFn() {
		if (this.webContents.canGoForward()) this.emit('gbf:canForward:ui', true);
		else this.emit('gbf:canForward:ui', false);

		if (this.webContents.canGoBack()) this.emit('gbf:canBackward:ui', true);
		else this.emit('gbf:canBackward:ui', false);
	}

	private processTilteChange(ev: Event, title: string) {
		if (!/-|\[|\]/ig.test(title)) return;
		let parsedTitle = title.split('-')[1].substr('['.length);
		parsedTitle = parsedTitle.substr(0, parsedTitle.length - ']'.length);
		const event = parsedTitle.split(':')[0];
		const data = parsedTitle.substr(event.length + ':'.length);

		switch (event) {
			case 'invokeUI':
				this.emit('ui:invoke');
				break;
			case 'transfer':
				this.emit('gbf:transfer:ui', JSON.parse(data));
				break;
			default:
				break;
		}
	}

	/**
	 * 使用修改文档title的方式唤醒UI
	 */
	private initUiAttach(): void {
		// prelaunch
		this.initUiAttachFn();

		this.on('page-title-updated', this.processTilteChange);

		this.webContents.on('did-finish-load', () => {
			this.webContents.executeJavaScript(`
				window.addEventListener('hashchange', (ev) => {
					document.title += '-[transfer:' + JSON.stringify(window.location) + ']';
					document.title = document.title.split('-')[0];
				})
			`);
			this.webContents.executeJavaScript(`
				const ui = document.createElement('li');
				const span = document.createElement('span');
				const injectElem = document.querySelector('[data-menubar-sidebutton="Home"]');
				ui.classList.add(injectElem.classList[1]);
				ui.setAttribute('data-menubar-sidebutton', 'Joybook');
				span.innerText = 'JoyBook UI';
				ui.appendChild(span);
				ui.addEventListener('click', () => {
					document.title += '-[invokeUI]';
					document.title = document.title.split('-')[0];
				});
				ui.style.color = 'white'
				injectElem.insertAdjacentElement('afterend', ui);
			`);
		});
		this.webContents.on('did-navigate-in-page', () => this.initUiAttachFn());
		this.webContents.on('did-start-loading', () => this.initUiAttachFn());
	}
}
