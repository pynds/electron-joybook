import {
	BrowserWindow,
	BrowserWindowConstructorOptions,
	Event,
	ipcMain,
} from 'electron';
import Store from 'electron-store';
import Window from './Window';

const layout = new Store({
	name: 'applayout',
});

export default class UiWindow extends Window {
	constructor(options: BrowserWindowConstructorOptions) {
		super(options);

		this.initEventListener();
		this.uiAdditionalEventListener();
	}

	public onResize(): void {
		const size = {
			width: this.getSize()[0],
			height: this.getSize()[1],
		};
		layout.set('uiWindow.size', size);
	}

	public onMove(): void {
		const position = {
			x: this.getPosition()[0],
			y: this.getPosition()[1],
		};
		layout.set('uiWindow.position', position);
	}

	public onMaximize(): void {
		this.webContents.send('ui:maximize:ui', this.isMaximized());
	}

	public onUnmaximize(): void {
		this.webContents.send('ui:unmaximize:ui', this.isMaximized());
	}

	private uiAdditionalEventListener(): void {
		const fn = (event: string): void => { this.webContents.send(`ui:${event}`); };
		this.on('focus', () => fn('focus'));
		this.on('blur', () => fn('blur'));
	}
}
