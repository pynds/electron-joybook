import { BrowserWindow, BrowserWindowConstructorOptions } from 'electron';
import { debounce } from 'throttle-debounce';

// 更符合typescript标准
// 更改为抽象类 https://www.tslang.cn/docs/handbook/classes.html
export default abstract class Window extends BrowserWindow {

	protected constructor(options: BrowserWindowConstructorOptions) {
		super(options);
	}

	public initEventListener(): void {
		this.on('resize', debounce(300, this.onResize));
		this.on('move', debounce(300, this.onMove));
		this.on('maximize', debounce(300, this.onMaximize));
		this.on('unmaximize', debounce(300, this.onUnmaximize));
	}

	public abstract onResize(): void;
	public abstract onMove(): void;
	public abstract onMaximize(): void;
	public abstract onUnmaximize(): void;
}
